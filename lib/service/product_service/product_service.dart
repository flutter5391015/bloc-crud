import 'dart:convert';

import 'package:bloc_hyperextension_example/models/product/product.dart';
import 'package:http/http.dart' as http;

class ProductService {
  Future<List<Product>> getData() async {
    var response = await http.get(
      Uri.parse("https://reqres.in/api/users"),
      headers: {
        "Content-Type": "application/json",
      },
    );
    Map obj = jsonDecode(response.body);
    List<Product> items =
        (obj['data'] as List).map((item) => Product.fromJson(item)).toList();

    return items;
  }

  Future add({required Product product}) async {
    await http.post(
      Uri.parse("https://reqres.in/api/users"),
      headers: {
        "Content-Type": "application/json",
      },
      body: jsonEncode({
        "photo": product.photo,
        "description": product.description,
        "name": product.name,
      }),
    );
  }

  Future update({required Product product}) async {
    var response = await http.put(
      Uri.parse("https://reqres.in/api/users/${product.id}"),
      headers: {
        "Content-Type": "application/json",
      },
      body: jsonEncode({
        "photo": product.photo,
        "description": product.description,
        "name": product.name,
      }),
    );
    Map obj = jsonDecode(response.body);
  }

  Future delete({required int id}) async {
    await http.delete(
      headers: {
        "Content-Type": "application/json",
      },
      Uri.parse("https://reqres.in/api/users/$id"),
    );
  }
}
