import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../bloc/product_from_bloc.dart';
import '../event/product_from_event.dart';
import '../state/product_from_state.dart';

class ProductFromView extends StatefulWidget {
  const ProductFromView({Key? key}) : super(key: key);

  @override
  State<ProductFromView> createState() => _ProductFromViewState();
}

class _ProductFromViewState extends State<ProductFromView> {
  ProductFromBloc bloc = ProductFromBloc();

  @override
  void initState() {
    bloc.initState();
    super.initState();
  }

  @override
  void dispose() {
    bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) => bloc,
      child: BlocListener<ProductFromBloc, ProductFromState>(
        listener: (context, state) {},
        child: BlocBuilder<ProductFromBloc, ProductFromState>(
          builder: (context, state) {
            final bloc = context.read<ProductFromBloc>();
            return buildView(context, bloc, state);
          },
        ),
      ),
    );
  }

  buildView(
    BuildContext context,
    ProductFromBloc bloc,
    ProductFromState state,
  ) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('ProductFrom'),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Text(
            'Counter: ${state.counter}',
            style: const TextStyle(fontSize: 24),
          ),
          IconButton(
            onPressed: () => bloc.add(ProductFromIncrementEvent()),
            icon: const Icon(
              Icons.add,
              size: 24.0,
            ),
          ),
        ],
      ),
    );
  }
}
