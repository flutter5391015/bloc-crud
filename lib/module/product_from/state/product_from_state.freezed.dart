// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'product_from_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$ProductFromState {
  int get counter => throw _privateConstructorUsedError;
  set counter(int value) => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $ProductFromStateCopyWith<ProductFromState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ProductFromStateCopyWith<$Res> {
  factory $ProductFromStateCopyWith(
          ProductFromState value, $Res Function(ProductFromState) then) =
      _$ProductFromStateCopyWithImpl<$Res, ProductFromState>;
  @useResult
  $Res call({int counter});
}

/// @nodoc
class _$ProductFromStateCopyWithImpl<$Res, $Val extends ProductFromState>
    implements $ProductFromStateCopyWith<$Res> {
  _$ProductFromStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? counter = null,
  }) {
    return _then(_value.copyWith(
      counter: null == counter
          ? _value.counter
          : counter // ignore: cast_nullable_to_non_nullable
              as int,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_ProductFromStateCopyWith<$Res>
    implements $ProductFromStateCopyWith<$Res> {
  factory _$$_ProductFromStateCopyWith(
          _$_ProductFromState value, $Res Function(_$_ProductFromState) then) =
      __$$_ProductFromStateCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({int counter});
}

/// @nodoc
class __$$_ProductFromStateCopyWithImpl<$Res>
    extends _$ProductFromStateCopyWithImpl<$Res, _$_ProductFromState>
    implements _$$_ProductFromStateCopyWith<$Res> {
  __$$_ProductFromStateCopyWithImpl(
      _$_ProductFromState _value, $Res Function(_$_ProductFromState) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? counter = null,
  }) {
    return _then(_$_ProductFromState(
      counter: null == counter
          ? _value.counter
          : counter // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$_ProductFromState implements _ProductFromState {
  _$_ProductFromState({this.counter = 0});

  @override
  @JsonKey()
  int counter;

  @override
  String toString() {
    return 'ProductFromState(counter: $counter)';
  }

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ProductFromStateCopyWith<_$_ProductFromState> get copyWith =>
      __$$_ProductFromStateCopyWithImpl<_$_ProductFromState>(this, _$identity);
}

abstract class _ProductFromState implements ProductFromState {
  factory _ProductFromState({int counter}) = _$_ProductFromState;

  @override
  int get counter;
  set counter(int value);
  @override
  @JsonKey(ignore: true)
  _$$_ProductFromStateCopyWith<_$_ProductFromState> get copyWith =>
      throw _privateConstructorUsedError;
}
