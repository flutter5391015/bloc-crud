import 'package:freezed_annotation/freezed_annotation.dart';
part 'product_from_state.freezed.dart';

@unfreezed
class ProductFromState with _$ProductFromState {
  factory ProductFromState({
    @Default(0) int counter,
  }) = _ProductFromState;
}
