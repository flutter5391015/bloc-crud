import 'package:flutter_bloc/flutter_bloc.dart';
import '../event/product_from_event.dart';
import '../state/product_from_state.dart';

mixin _BlocLifecycle {
  void initState() {}
  void dispose() {}
}

class ProductFromBloc extends Bloc<ProductFromEvent, ProductFromState>
    with _BlocLifecycle {
  ProductFromBloc() : super(ProductFromState()) {
    on<ProductFromIncrementEvent>((event, emit) {
      state.counter++;
      emit(state.copyWith());
    });
  }

  @override
  void initState() {
    //initState event
    super.initState();
  }

  @override
  void dispose() {
    //dispose event
    super.dispose();
  }

  @override
  Future<void> close() {
    dispose();
    return super.close();
  }
}
