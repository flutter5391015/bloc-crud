import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../bloc/single_selection_bloc.dart';
import '../event/single_selection_event.dart';
import '../state/single_selection_state.dart';

class SingleSelectionView extends StatefulWidget {
  const SingleSelectionView({Key? key}) : super(key: key);

  @override
  State<SingleSelectionView> createState() => _SingleSelectionViewState();
}

class _SingleSelectionViewState extends State<SingleSelectionView> {
  SingleSelectionBloc bloc = SingleSelectionBloc();

  @override
  void initState() {
    bloc.initState();
    super.initState();
  }

  @override
  void dispose() {
    bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) => bloc,
      child: BlocListener<SingleSelectionBloc, SingleSelectionState>(
        listener: (context, state) {},
        child: BlocBuilder<SingleSelectionBloc, SingleSelectionState>(
          builder: (context, state) {
            final bloc = context.read<SingleSelectionBloc>();
            return buildView(context, bloc, state);
          },
        ),
      ),
    );
  }

  buildView(
    BuildContext context,
    SingleSelectionBloc bloc,
    SingleSelectionState state,
  ) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('SingleSelection'),
      ),
      body: ListView.separated(
        separatorBuilder: (BuildContext context, int index) {
          return const SizedBox(height: 10);
        },
        itemCount: state.items.length,
        physics: const ScrollPhysics(),
        padding: const EdgeInsets.all(16),
        itemBuilder: (BuildContext context, int index) {
          var item = state.items[index];

          return Card(
            child: ListTile(
              title: Text(item['label']),
              trailing: GestureDetector(
                onTap: () => bloc.add(
                  SingleSelectionAddIndexEvent(index: index),
                ),
                child: Icon(
                  state.selectedIndex == index
                      ? Icons.check_box_outlined
                      : Icons.check_box_outline_blank,
                  size: 24.0,
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
