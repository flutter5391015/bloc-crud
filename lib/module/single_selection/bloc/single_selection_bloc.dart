import 'package:flutter_bloc/flutter_bloc.dart';
import '../event/single_selection_event.dart';
import '../state/single_selection_state.dart';

mixin _BlocLifecycle {
  void initState() {}
  void dispose() {}
}

class SingleSelectionBloc
    extends Bloc<SingleSelectionEvent, SingleSelectionState>
    with _BlocLifecycle {
  SingleSelectionBloc() : super(SingleSelectionState()) {
    on<SingleSelectionAddIndexEvent>((event, emit) {
      state.selectedIndex = event.index;
      emit(state.copyWith());
    });
  }

  @override
  void initState() {
    //initState event
    state.items = [
      {"label": "Male", "value": "male"},
      {"label": "Female", "value": "female"},
    ];
    super.initState();
  }

  @override
  void dispose() {
    //dispose event
    super.dispose();
  }

  @override
  Future<void> close() {
    dispose();
    return super.close();
  }
}
