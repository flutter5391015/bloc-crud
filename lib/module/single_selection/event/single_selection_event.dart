abstract class SingleSelectionEvent {}

class SingleSelectionAddIndexEvent extends SingleSelectionEvent {
  final int index;
  SingleSelectionAddIndexEvent({required this.index});
}
