import 'package:freezed_annotation/freezed_annotation.dart';
part 'single_selection_state.freezed.dart';

@unfreezed
class SingleSelectionState with _$SingleSelectionState {
  factory SingleSelectionState({
    @Default(0) int selectedIndex,
    @Default([]) List items,
  }) = _SingleSelectionState;
}
