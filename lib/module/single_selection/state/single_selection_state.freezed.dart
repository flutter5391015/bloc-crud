// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'single_selection_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$SingleSelectionState {
  int get selectedIndex => throw _privateConstructorUsedError;
  set selectedIndex(int value) => throw _privateConstructorUsedError;
  List<dynamic> get items => throw _privateConstructorUsedError;
  set items(List<dynamic> value) => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $SingleSelectionStateCopyWith<SingleSelectionState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SingleSelectionStateCopyWith<$Res> {
  factory $SingleSelectionStateCopyWith(SingleSelectionState value,
          $Res Function(SingleSelectionState) then) =
      _$SingleSelectionStateCopyWithImpl<$Res, SingleSelectionState>;
  @useResult
  $Res call({int selectedIndex, List<dynamic> items});
}

/// @nodoc
class _$SingleSelectionStateCopyWithImpl<$Res,
        $Val extends SingleSelectionState>
    implements $SingleSelectionStateCopyWith<$Res> {
  _$SingleSelectionStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? selectedIndex = null,
    Object? items = null,
  }) {
    return _then(_value.copyWith(
      selectedIndex: null == selectedIndex
          ? _value.selectedIndex
          : selectedIndex // ignore: cast_nullable_to_non_nullable
              as int,
      items: null == items
          ? _value.items
          : items // ignore: cast_nullable_to_non_nullable
              as List<dynamic>,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_SingleSelectionStateCopyWith<$Res>
    implements $SingleSelectionStateCopyWith<$Res> {
  factory _$$_SingleSelectionStateCopyWith(_$_SingleSelectionState value,
          $Res Function(_$_SingleSelectionState) then) =
      __$$_SingleSelectionStateCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({int selectedIndex, List<dynamic> items});
}

/// @nodoc
class __$$_SingleSelectionStateCopyWithImpl<$Res>
    extends _$SingleSelectionStateCopyWithImpl<$Res, _$_SingleSelectionState>
    implements _$$_SingleSelectionStateCopyWith<$Res> {
  __$$_SingleSelectionStateCopyWithImpl(_$_SingleSelectionState _value,
      $Res Function(_$_SingleSelectionState) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? selectedIndex = null,
    Object? items = null,
  }) {
    return _then(_$_SingleSelectionState(
      selectedIndex: null == selectedIndex
          ? _value.selectedIndex
          : selectedIndex // ignore: cast_nullable_to_non_nullable
              as int,
      items: null == items
          ? _value.items
          : items // ignore: cast_nullable_to_non_nullable
              as List<dynamic>,
    ));
  }
}

/// @nodoc

class _$_SingleSelectionState implements _SingleSelectionState {
  _$_SingleSelectionState({this.selectedIndex = 0, this.items = const []});

  @override
  @JsonKey()
  int selectedIndex;
  @override
  @JsonKey()
  List<dynamic> items;

  @override
  String toString() {
    return 'SingleSelectionState(selectedIndex: $selectedIndex, items: $items)';
  }

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_SingleSelectionStateCopyWith<_$_SingleSelectionState> get copyWith =>
      __$$_SingleSelectionStateCopyWithImpl<_$_SingleSelectionState>(
          this, _$identity);
}

abstract class _SingleSelectionState implements SingleSelectionState {
  factory _SingleSelectionState({int selectedIndex, List<dynamic> items}) =
      _$_SingleSelectionState;

  @override
  int get selectedIndex;
  set selectedIndex(int value);
  @override
  List<dynamic> get items;
  set items(List<dynamic> value);
  @override
  @JsonKey(ignore: true)
  _$$_SingleSelectionStateCopyWith<_$_SingleSelectionState> get copyWith =>
      throw _privateConstructorUsedError;
}
